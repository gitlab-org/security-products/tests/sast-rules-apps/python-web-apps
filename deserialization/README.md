# python-web-apps
## Insecure Deserialization with Pickle

This is a simple flask based app, that has a primitive form of wizard while storing the data in the cookie and 
serializing it. You can run through each of the steps and then see all the data at the end.


Run app using the following command:
```
python3 SerializationWithPickle.py
```

To demonstrate the vulnerability, a malicious object is created on the server when you make a get request to "/exploit",
which will then set the malicious object in the cookie and run the command when the cookie is deserialized in the next 
request, the contents of the output from that command is shown when a get request is made to "/finish".

Rule Link : 
https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/deserialization/rule-pickle.yml?ref_type=heads
