# python-web-apps
## pysnmp Weak Cyrptography


Run the app using the following command:
```
docker build -t my-python-app .  && docker run --rm -p 4000:4000 my-python-app
```

Rule Link : 
https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/snmp/rule-snmp-weak-cryptography.yml?ref_type=heads


FYI ->
The original pysnmp library is no longer maintained - https://github.com/etingof/pysnmp
(This is installed by `pip install pysnmp`)

The popular fork that has taken over this library is - https://github.com/pysnmp/pysnmp/tree/main
(This is installed by `pip install pysnmplib`)

The latest syntax of auth and priv constants is same for both. I have put both libraries in `requirements.txt`. You can keep any 1 of the 2 at a time and the script will run smoothly for both libraries.
