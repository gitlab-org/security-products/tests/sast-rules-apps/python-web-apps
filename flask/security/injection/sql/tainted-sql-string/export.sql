PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE user (
	id INTEGER NOT NULL, 
	firstname VARCHAR(100) NOT NULL, 
	lastname VARCHAR(100) NOT NULL, 
	email VARCHAR(80) NOT NULL, 
	age INTEGER, 
	created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), 
	PRIMARY KEY (id), 
	UNIQUE (email)
);
INSERT INTO user VALUES(1,'john','doe','jd@example.com',33,'2024-03-29 15:12:24');
INSERT INTO user VALUES(2,'sam','doe','sam@example.com',40,'2024-03-29 15:12:24');
INSERT INTO user VALUES(3,'carl','doe','carl@example.com',22,'2024-03-29 15:12:24');
INSERT INTO user VALUES(4,'niels','doe','niels@example.com',18,'2024-03-29 15:12:24');
COMMIT;
