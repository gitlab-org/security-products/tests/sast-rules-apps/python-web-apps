
# Testing inter-procedural and inter-file tainted query returned
def inter_file_query_builder(myrequest=None):
    if myrequest is None:
        query = "SELECT * FROM myapp_user WHERE username = 'testuser1'"
    else:
        uname = myrequest.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
    return query

# Testing inter-procedural and inter-file non-tainted returned value
def inter_file_safe_function(param=None):
    retval = "SELECT * FROM myapp_user WHERE username = 'testuser1'"
    return retval

