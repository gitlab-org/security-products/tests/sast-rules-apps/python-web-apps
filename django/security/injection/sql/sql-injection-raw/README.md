# SAST Vulnerable Minimal Runnable Example(MRE)

## Background Information

This MRE portrays the realisitc but improper usage of [Django SQL Injection using raw() function](https://docs.djangoproject.com/en/5.0/topics/db/sql/#performing-raw-queries).

### References

- [Django Raw SQL queries](https://docs.djangoproject.com/en/5.0/topics/db/sql/#performing-raw-sql-queries)
- [Advanced SQL injection cheatsheet](https://github.com/kleiton0x00/Advanced-SQL-Injection-Cheatsheet/tree/main) 


## Running

```
docker compose build && docker compose up
Navigate to: http://localhost:8000/myapp/
```

## DB Entries

For testing purposes:
```
(username='testuser1', email='test1@example.com')
(username='testuser2', email='test2@example.com')
(username='testuser3', email='test3@example.com')
```

## Normal Execution

Example:
```
$ curl 'http://localhost:8000/myapp/test_sql_injection1?username=testuser1'
```
Pre cooked links for all test cases are available at : 
`http://localhost:8000/myapp/`

## Exploitation 

```
$ curl 'http://localhost:8000/myapp/test_sql_injection1?username=%27%20or%20email=%27test3@example.com'
```

