from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="myapp"),
    path("testcase01_vulnerable/<int:id>/", views.testcase01_vulnerable, name="testcase01_vulnerable"),
    path("testcase02_vulnerable/<int:id>/", views.testcase02_vulnerable, name="testcase02_vulnerable"),
    path("testcase03_vulnerable/<int:id>/", views.testcase03_vulnerable, name="testcase03_vulnerable"),
    path("testcase04_vulnerable/<int:id>/", views.testcase04_vulnerable, name="testcase04_vulnerable"),
    path("testcase07_vulnerable/<int:id>/", views.testcase07_vulnerable, name="testcase07_vulnerable"),
    path("testcase08_vulnerable/<int:id>/", views.testcase08_vulnerable, name="testcase08_vulnerable"),
    path("testcase11_safe/<int:id>/", views.testcase11_safe, name="testcase11_safe"),
    path("testcase12_vulnerable/<int:id>/", views.testcase12_vulnerable, name="testcase12_vulnerable"),
    path("testcase17_vulnerable/<int:id>/<path:path>", views.testcase17_vulnerable, name="testcase17_vulnerable"),

    path("testcase05_safe/<int:id>/", views.testcase05_safe, name="testcase05_safe"),
    path("testcase06_safe/<int:id>/", views.testcase06_safe, name="testcase06_safe"),
    path("testcase09_safe/<int:id>/", views.testcase09_safe, name="testcase09_safe"),
    path("testcase10_safe/<int:id>/", views.testcase10_safe, name="testcase10_safe"),
    path("testcase13_vulnerable/<int:id>/", views.testcase13_vulnerable, name="testcase13_vulnerable"),
    path("testcase14_safe/<int:id>/", views.testcase14_safe, name="testcase14_safe"),
    path("testcase15_safe/<int:id>/", views.testcase15_safe, name="testcase15_safe"),
    path("testcase16_safe/<int:id>/", views.testcase16_safe, name="testcase16_safe"),

]
