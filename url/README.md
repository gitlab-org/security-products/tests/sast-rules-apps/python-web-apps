# python-web-apps
## URL handling with urllib

The InsecureUrlProcessingWithUrllib.py script is a simple Flask web application that handles URLs and files in an insecure manner. It has several routes that perform different actions:

- The /download_html route accepts a URL via a POST request, downloads the CONTENT at that URL, saves it to an HTML file, and sends the file as a response. After the file is sent, it is deleted from the server.

- The /download_file route accepts a URL via a POST request, downloads the FILE at that URL, and sends the file as a response. After the file is sent, it is deleted from the server.

- The /exploit_url route reveals a string that you can use with /download_file to download the secret file on the server.

Run the app using the following command:
```
python3 InsecureUrlProcessingWithUrllib.py
```

For the python2 variant use the following command:
```
python py2_InsecureUrlProcessingWithUrllib.py
```

To demonstrate the vulnerability, a URL payload is created on the server when you make a get request to "/exploit_url" and it is sent back in the response. Use this payload for "Download a file from URL" and a secret file from the server will be downloaded.

Rule Link : 
https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/urlopen/rule-urllib-urlopen.yml?ref_type=heads
