# Pre-requisites:
# Install the required packages using below commands:
# `pip3 install urllib`
# `pip3 install flask`

import os
import time
import urllib2
import ssl

from flask import Flask, request, send_file, after_this_request

app = Flask(__name__)

ssl._create_default_https_context = ssl._create_unverified_context


def render_form(error=''):
    return '''
        <h2 style="color:red">%s</h2>
        <br>
        <h1>Download a file from URL</h1>
        <form method="POST" action="/download_file">
            URL: <input type="text" name="url">
            <input type="submit" value="Download File!">
        </form>
        <br>
        <br>
        <h1>Download HTML from a URL</h1>
        <form method="POST" action="/download_html">
            URL: <input type="text" name="url">
            <input type="submit" value="Download HTML!">
        </form>
    ''' % error


def download_from_url(url, filename):
    epoch_time = str(int(time.time()))
    file = os.path.join(os.getcwd(), filename % epoch_time)

    req = urllib2.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req)
    html = response.read()
    with open(file, 'wb') as out_file:
        out_file.write(html)

    return file


@app.route('/')
def home():
    return render_form()


@app.route('/download_file', methods=['POST'])
def download_file():
    url = request.form.get('url')

    if not url:
        return render_form(error='URL is not provided!')

    _, ext = os.path.splitext(url)

    if not ext:
        return render_form(error='File extension is not provided in the URL!')

    filename = download_from_url(url, '%s' + ext)

    # This removes the file after the response is sent.
    @after_this_request
    def delete_file(response):
        try:
            os.remove(filename)
        except OSError as error:
            print("Error removing or closing downloaded file handle: %s", error)
        return response

    return send_file(filename, as_attachment=True)


@app.route('/download_html', methods=['POST'])
def download_html():
    url = request.form.get('url')

    if not url:
        return render_form(error='URL is not provided!')

    filename = download_from_url(url, '%s.html')
    
    @after_this_request
    def remove_file(response):
        try:
            os.remove(filename)
        except Exception as error:
            print("Error removing or closing downloaded file handle: %s", error)
        return response
    
    return send_file(filename, as_attachment=True)


# For the purpose of demonstration of the vulnerability we are exposing the secret.txt file path.
# So that the user can see how file:// protocol can be exploited.
@app.route('/exploit_url', methods=['GET'])
def exploit_url():
    file_path = 'file://' + os.path.abspath('secret.txt')
    return render_form(error='Secret file path: %s' % file_path)


def create_secret():
    filename = 'secret.txt'
    if not os.path.exists(filename):
        with open(filename, 'w') as f:
            f.write('This is a secret file.')
    file_path = os.path.abspath(filename)
    url = 'file://' + file_path
    print(url)


if __name__ == '__main__':
    create_secret()
    app.run(debug=True)
