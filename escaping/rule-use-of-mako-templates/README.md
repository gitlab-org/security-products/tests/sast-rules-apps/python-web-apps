# python-web-apps
## Cross-Site Scripting using Mako Templates

This is a simple Flask based web application that demonstrates the use of Mako templates. The application allows users 
to submit messages with a chosen background color, which are then saved in a file "data.json" on the server which serves 
as a datastore. When the homepage is viewed the messages are loaded from the datastore and displayed on the message board.

## Run using Docker
```
docker-compose build

docker-compose up
```
To view the site, browse to "http://127.0.0.1:5000"


## Run app without Docker:
```
python3 -m venv .venv
source .venv/bin/activate
pip3 install mako flask
python3 anonymous_message_board.py
```
To view the site, browse to "http://127.0.0.1:5000"


To demonstrate the vulnerability. You can either browse the "/exploit" endpoint or click on the devil icon below the title. 
This will send a malicious XSS payload as a message to be displayed on the message board. And when you go back to the 
homepage, you can see the exploit execute, demonstrating the vulnerability.

To view a secure version of the app, you can either browse the "/secure" endpoint or click on the locked padlock icon 
below the title. To go back to the insecure version "homepage", you can either click the unlocked padlock icon or browse 
the "/" endpoint.

Rule Link : 
https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/escaping/rule-use-of-mako-templates.yml?ref_type=heads
