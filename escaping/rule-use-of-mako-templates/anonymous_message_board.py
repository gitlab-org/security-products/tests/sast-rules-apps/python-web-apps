# Pre-requisites:
# Install the required packages using below commands:
# python3 -m venv .venv
# source .venv/bin/activate
# pip3 install mako flask

import os
import json

from flask import Flask, request, redirect, url_for
from mako.lookup import TemplateLookup
from mako.template import Template
from mako import exceptions

app = Flask(__name__)
datastore = os.path.join(os.path.dirname(__file__), 'data.json')

# Configure Mako templates for Flask
# ruleid: python_escaping_rule-use-of-mako-templates
lookup = TemplateLookup(directories=[os.path.join(os.path.dirname(__file__), 'templates')])
# rule ok: python_escaping_rule-use-of-mako-templates
lookupSecure = TemplateLookup(directories=[os.path.join(os.path.dirname(__file__), 'templates')], default_filters=['h'])


def save_message(message, color):
    # Save data to file
    data = {'message': message, 'background_color': color}
    with open(datastore, 'a') as f:
        f.write(json.dumps(data) + '\n')


def load_messages():
    # Load data from file
    messages = []
    if os.path.exists(datastore):
        with open(datastore, 'r') as f:
            for line in f:
                messages.append(json.loads(line.strip()))
    return messages


@app.route('/submit', methods=['POST'])
def submit():
    # Get user input from form
    user_message = request.form.get('user_message')
    background_color = request.form.get('background_color')

    # Save message to file
    save_message(user_message, background_color)

    # Redirect to index
    return redirect(url_for('index'))


@app.route('/')
def index():
    # Load messages from datastore
    messages = load_messages()

    # Render template with data
    try:
        template = lookup.get_template('index.mako')
        return template.render(data=messages)
    except:
        return exceptions.html_error_template().render()


@app.route('/secure')
def secure():
    # Load messages from datastore
    messages = load_messages()

    # Render template with data
    try:
        # Use lookupSecure so that templates are escaped by default
        template = lookupSecure.get_template('index.mako')
        return template.render(data=messages)
    except:
        return exceptions.html_error_template().render()


@app.route('/exploit')
def exploit():
    # Add XSS payload message to datastore
    save_message("<div> <h1>XSS Executed!</h1> <img src=x onerror=alert(document.domain)> </div>", "#1268bd")
    filename = os.path.join(os.path.dirname(__file__), 'exploit.mako')

    # Render template to tell user that exploit is loaded
    try:
        # ruleid: python_escaping_rule-use-of-mako-templates
        template = Template(filename=filename)
        return template.render()
    except:
        return exceptions.html_error_template().render()


if __name__ == "__main__":
    app.run(debug=True)
