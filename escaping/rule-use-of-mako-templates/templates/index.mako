<!DOCTYPE html>
<html>
<head>
    <title>Anonymous Message Board</title>
    <style>
        body {
            text-align: center;
            font-family: Arial, sans-serif;
        }
        .form-section, .card-section {
            margin-top: 20px;
        }
        .card {
            display: inline-block;
            margin: 10px;
            padding: 20px;
            border-radius: 5px;
            width: 250px;
            height: 200px;
            text-align: center;
            vertical-align: top;
        }
    </style>
</head>
<body>
    <h1>
        Anonymous Message Board
    </h1>
    <a href="/" style="text-decoration:none" title="Click me to load the insecure version">🔓 Insecure Version</a> |
    <a href="/secure" style="text-decoration:none" title="Click me to load the secure version">🔒 Secure Version</a> |
    <a href="/exploit" style="text-decoration:none" title="Click me to load the exploit">😈 Load Exploit</a>
    <hr>
    <div class="form-section">
        <%include file="form.mako"/>
    </div>
    <hr>
    <div class="card-section">
        <%include file="cards.mako"/>
    </div>
</body>
</html>