<form action="/submit" method="post" onsubmit="return validateForm()">
    <label for="background_color">Choose a background color:</label>
    <select name="background_color" id="background_color">
        <option value="#f8f9fa">Light</option>
        <option value="#d6e0f0">Blue</option>
        <option value="#f0d6e0">Pink</option>
        <option value="#e0f0d6">Green</option>
        <option value="#f0f0d6">Yellow</option>
        <option value="#f0f0f0">White</option>
        <option value="#d6f0f0">Cyan</option>
        <option value="#f0d6f0">Magenta</option>
    </select>
    <br/>
    <br/>
    <label for="user_message">Enter your message:</label>
    <input type="text" name="user_message" id="user_message" placeholder="Enter your message">
    <br/>
    <br/>
    <input type="submit" value="Submit">
</form>

<script>
function validateForm() {
    var x = document.getElementById("user_message").value;
    if (x.trim() == "") {
        alert("Message must be filled out");
        return false;
    }
}
</script>